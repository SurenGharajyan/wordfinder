export class Builder {
    public static game: Phaser.Game;

    public static initialize(game: Phaser.Game) {
        Builder.game = game;
    }
}