export class UserData {
    private _isFirstTime: boolean;
    private _score: number;
    private _money: number;

    get isFirstTime(): boolean {
        return this._isFirstTime;
    }

    set isFirstTime(value: boolean) {
        this._isFirstTime = value;
    }

    get score(): number {
        return this._score;
    }

    set score(value: number) {
        this._score = value;
    }

    get money(): number {
        return this._money;
    }

    set money(value: number) {
        this._money = value;
    }

}