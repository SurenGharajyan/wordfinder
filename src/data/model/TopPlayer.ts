export class TopPlayer {
    private _emoji: string;
    private _name: string;
    private _place: string;
    private _score: number;

    constructor(emoji: string, name: string, place: string, score: number) {
        this._emoji = emoji;
        this._name = name;
        this._place = place;
        this._score = score;
    }

    public get emoji(): string {
        return this._emoji;
    }

    public get name(): string {
        return this._name;
    }

    public get place(): string {
        return this._place;
    }

    public get score(): number {
        return this._score;
    }


}