import {BaseScreen} from '../game/screens/BaseScreen';

export class ScreenManager {
    private static _i: ScreenManager;

    public static get i(): ScreenManager {
        if (ScreenManager._i == null) {
            ScreenManager._i = new ScreenManager();
        }
        return ScreenManager._i;
    }

    private _currentScreen: BaseScreen;

    public get currentScreen(): BaseScreen {
        return this._currentScreen;
    }

    public show(screen: BaseScreen): void {
        this._currentScreen = screen;
    }

}