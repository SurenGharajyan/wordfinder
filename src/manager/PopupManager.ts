import {Builder} from '../build/Builder';

export class PopupManager extends Phaser.Group {
    private static _i: PopupManager;

    public static get i(): PopupManager {
        if (PopupManager._i == null) {
            PopupManager._i = new PopupManager();
        }
        return PopupManager._i;
    }

    constructor(parent?: Phaser.Group) {
        super(Builder.game,parent);

    }

    public showPopup(popup: PopupManager, x: number, y: number, context?: any): void {
        popup.position.set(x, y);
        popup.alpha = 0;
        const tween = this.game.add.tween(popup)
            .to({alpha: 1}, 150, null, true);
        tween.onComplete.addOnce(() => {
                popup.alpha = 1;
            }
        );
    }

    public hidePopup(popup: PopupManager): void {
        const tween = this.game.add.tween(popup.alpha)
            .to({alpha: 0}, 150, null, true);
        tween.onComplete.addOnce(() => {
                popup.alpha = 0;
                popup.parent.removeChild(popup);
            }
        );
    }
}