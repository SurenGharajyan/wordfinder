import Color = Phaser.Color;
import {Images} from '../../../assets';
import {Config} from "../configuration/Config";
import {ScrollBar} from "../configuration/ScrollBar";

export class Scroll extends Phaser.Group {
    private groupOfElements : Phaser.Group;
    private groupScrollBtn : Phaser.Group;
    private element : Phaser.Group;
    private scrollArea : Phaser.Sprite;
    private maskOfElements: Phaser.Graphics;
    private maskYPos : number;
    private maskHeight : number;
    private isMaskClicked : boolean;
    private scrollBtnHeight : number;
    private scrollSpeed : number;
    private mouseLastPos : number = 0;
    private mouseOldY : number = 0;
    private percent : number = 0;

    constructor(g, array : Phaser.Group[], config : Config) {
        super(g);
        this.init(array,config);
    }

    private init(arr : Phaser.Group[], config : Config) : void {
        this.initElements(arr,config);
        this.attachListeners();
    }

    private initElements(elements : Phaser.Group[], config : Config) : void {
        this.groupOfElements = this.game.add.group();
        this.groupOfElements.scale.set(0.5);
        for (let i = 0; i < elements.length ; i++) {
            this.element = elements[i];
            this.groupOfElements.addChild(this.game.add.text( this.element.width + 10,
                (this.element.centerY), (i + 1) + ''));
            this.groupOfElements.addChild(this.element);
        }
        this.groupOfElements.x = config.position.x;
        this.initMask(config.position.y);
        this.groupOfElements.y = this.maskOfElements.y;
        this.groupOfElements.width = config.widthGroup;
    }

    private initMask(y : number) : void {
        //CHANGE VALUE CUSTOM HERE
        this.maskYPos = y;
        this.maskHeight = this.game.world.centerY + 2 * this.game.world.centerY/3;

        this.maskOfElements = this.game.add.graphics(this.groupOfElements.x, this.maskYPos,this);
        this.maskOfElements.beginFill(Color.AQUA);
        this.maskOfElements.drawRect(0,0,
            this.groupOfElements.width + 10, this.maskHeight );
        this.maskOfElements.endFill();
        this.groupOfElements.mask = this.maskOfElements;
        this.maskOfElements.inputEnabled = true;
        this.maskOfElements.events.onInputDown.add(this.maskClick, this);
        this.maskOfElements.events.onInputUp.add(this.maskUp, this);
    }

    private attachListeners() : void {
        this.game.input.onDown.add(this.mouseFirstClick,this);
        this.scrollUp.inputEnabled = true;
        this.scrollDown.inputEnabled = true;
        this.scrollUp.events.onInputDown.add(this.upClicked,this);
        this.scrollDown.events.onInputDown.add(this.downClicked,this);
        window.addEventListener('wheel', () => {
            this.mouseWheel();
        });
    }

    private mouseFirstClick() : void {
        this.mouseOldY = this.game.input.mousePointer.y;
    }

    private mouseWheel() : void {
        if (this.game.input.mouse.wheelDelta === Phaser.Mouse.WHEEL_UP
            && (this.maskOfElements.getBounds().contains(this.game.input.activePointer.worldX, this.game.input.activePointer.worldY))
        ) {
            if (this.scrollBtn.y > 0) {
                // if (this.scrollBtn.y <= this.scrollArea.y) {
                //     this.scrollBtn.y = this.scrollUp.height;
                //     this.groupOfElements.y = this.maskOfElements.y;
                // } else {
                this.groupOfElements.y += this.scrollSpeed;
                this.posGroupConvertScrollBtn();
            }
        } else if (this.game.input.mouse.wheelDelta === Phaser.Mouse.WHEEL_DOWN
            && (this.maskOfElements.getBounds().contains(this.game.input.activePointer.worldX, this.game.input.activePointer.worldY)
                || this.scrollGroup.getBounds().contains(this.game.input.activePointer.worldX, this.game.input.activePointer.worldY))) {
            if (this.scrollBtn.y + this.scrollBtnHeight < this.scrollArea.height) {
                // if (this.scrollBtn.y + this.scrollBtnHeight + this.scrollSpeed > this.scrollDown.y - this.scrollDown.height) {
                //     this.scrollBtn.y = this.scrollDown.y - this.scrollBtnHeight;
                //     this.groupOfElements.y = (this.maskOfElements.y + this.maskOfElements.height) - this.groupOfElements.height;
                // } else {
                this.groupOfElements.y -= this.scrollSpeed;
                this.posGroupConvertScrollBtn();
            }
        }
    }

    private maskClick() : void {
        this.isMaskClicked = true;
    }

    private maskUp() : void {
        this.isMaskClicked = false;
    }

    private posGroupConvertScrollBtn() : void {
        this.percent =  100 * (this.maskYPos - this.groupOfElements.y) / (this.groupOfElements.height - this.maskHeight);
        this.scrollBtn.y = (this.scrollArea.height - this.scrollBtnHeight ) * this.percent / 100 ;
    }

    public update() : void {
        if (this.game.input.mousePointer.isDown && this.isMaskClicked) {
            if (this.maskOfElements.getBounds().contains(this.game.input.mousePointer.x, this.game.input.mousePointer.y)) {
                if (((this.groupOfElements.y >= -this.groupOfElements.height + this.maskYPos + this.maskOfElements.height)
                    && (this.game.input.mousePointer.y - this.mouseOldY < 0))
                    ||
                    ((this.groupOfElements.y <= this.maskYPos)
                        && (this.game.input.mousePointer.y - this.mouseOldY) > 0)
                ) {
                    this.groupOfElements.y = this.mouseLastPos + this.game.input.mousePointer.y - this.mouseOldY;
                    if (this.groupOfElements.y > this.maskYPos) {
                        this.groupOfElements.y = this.maskYPos;
                    }
                    else if (this.groupOfElements.y + this.groupOfElements.height < this.maskYPos + this.maskHeight) {
                        this.groupOfElements.y = this.maskYPos + this.maskHeight - this.groupOfElements.height;
                    }
                }
                this.percent = 100 * (this.maskYPos - this.groupOfElements.y) / (this.groupOfElements.height - this.maskHeight);
                this.scrollBtn.y = (this.scrollDown.y - this.scrollDown.height - this.scrollBtnHeight) * this.percent / 100;
            }
        } else {
            this.mouseLastPos = this.groupOfElements.y;
        }
    }
}