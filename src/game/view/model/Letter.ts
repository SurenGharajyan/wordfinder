import {Atlases} from '../../../assets';
import {Util} from '../../../utils/util';
import MathUtils = Util.MathUtils;
import Group = Phaser.Group;

export class Letter extends Group {
    private _letterBgImage: Phaser.Sprite;
    private _letterSymbol: Phaser.Text;
    constructor(g: Phaser.Game, x: number, y: number,parent) {
        super(g,parent);
        this.initLetterBackground(x, y);
        this.initLetterText();
    }

    private initLetterBackground(x: number, y: number): void {
        this._letterBgImage = this.game.add.sprite(x
            , y, Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.UnselecedLetter,this
        )
    }

    private initLetterText(): void {
        this._letterSymbol = this.game.add.text(0,0,String.fromCharCode(MathUtils.getRandomInt(65, 90)).toString(),
            {fontSize: 43, font:'ArialRoundedMTBold'}, this);
        this._letterSymbol.x = this._letterBgImage.centerX - this._letterSymbol.width / 2;
        this._letterSymbol.y = this._letterBgImage.centerY - this._letterSymbol.height / 2;
    }

    get letterBgImage(): Phaser.Sprite {
        return this._letterBgImage;
    }

    get letterSymbol(): Phaser.Text {
        return this._letterSymbol;
    }

}