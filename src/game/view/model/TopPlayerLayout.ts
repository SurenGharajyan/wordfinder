import {TopPlayer} from '../../../data/model/TopPlayer';
import {Atlases} from '../../../assets';

export class TopPlayerLayout extends Phaser.Group{

    constructor(g: Phaser.Game, x: number, y: number, topPlayerData: TopPlayer, parent: Phaser.Group) {
        super(g,parent);

        //TODO this topPlayer object values are getting from server, but now for example I choose some randoms names
        this.init(x, y, topPlayerData);
    }

    private init(x: number, y: number, topPlayerData: TopPlayer): void {
        this.initPosition(x, y);
        this.initElements(topPlayerData);
    }

    private initPosition(x: number, y: number): void {
        this.x = x;
        this.y = y;
    }

    private initElements(topPlayerData: TopPlayer): void {
        const topPlayerBg = this.game.add.sprite(0, 0,
            Atlases.ImagesLeadboardPopup.getName(),
            Atlases.ImagesLeadboardPopup.Frames.ScoreTodayBestPlayers,
            this);

        // PSD file doesn't ready, for that reason I add different image
        const smile = this.game.add.sprite(14, 12, Atlases.ImagesLeadboardPopup.getName(),
            topPlayerData.emoji,
            this);

        // remove this line
        smile.scale.setTo(0.5);

        const playerName = this.game.add.text(70,16, topPlayerData.name,
            {fontSize: 18, font: 'ArialRoundedMTBold', fill: '#ffffff'},
            this
        );

        const place = this.game.add.text(207, 16, topPlayerData.place,
            {fontSize: 19, font: 'ArialRoundedMTBold', fill: '#ffffff'},
            this
        );

        const score = this.game.add.text(428, 16, topPlayerData.score.toString(),
            {fontSize: 20, font:'ArialRoundedMTBold', fill: '#ffffff'},
            this
        );
    }
}