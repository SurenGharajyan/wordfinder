import {Atlases} from '../../assets';
import Group = Phaser.Group;
import {GameController} from '../controller/GameController';

export class ScoreAndTimer extends Group {
    private timer: Phaser.Text;
    private _userScore: Phaser.Text;

    public get userScore(): Phaser.Text {
        return this._userScore;
    }

    constructor(g: Phaser.Game, x: number, y: number, gameController: GameController, parent: Phaser.Group) {
        super(g, parent);
        this.initPosition(x, y);
        this.initElements(gameController);

    }

    private initPosition(x: number, y: number): void {
        this.x = x;
        this.y = y;
    }

    private initElements(gameController: GameController): void {
        const trimLine = this.game.add.sprite(0,3, Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.LineTrim,this);

        const timerBgDark = this.game.add.sprite(0,10, Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.YouTimerBg, this);

        const timerBgSlight = this.game.add.sprite(0,14,Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.YouTimerTrimBg, this);

        const userScoreInfoBg = this.game.add.sprite(167,13,Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.YouBg, this);

        const userScoreTextYou = this.game.add.text(0,12,'You',
            {font: 'Tahoma',fill: '#ffffff', fontSize: 22}, this);
        userScoreTextYou.x = userScoreInfoBg.centerX - userScoreTextYou.width / 2;

        const userScoreBg = this.game.add.sprite(173,33,Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.YouScoreBg, this);

        this._userScore = this.game.add.text(0,33,'0',
            {font: 'Open 24 Display St',fill: '#ffa92a', fontSize: 26}, this);
        this._userScore.x = userScoreBg.centerX - this._userScore.width / 2;

        const timerBg = this.game.add.sprite(436,20,Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.TimerBg, this);

        const timerIcon = this.game.add.sprite(453,25,Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.Timer, this);

        this.timer = this.game.add.text(530,25,'2:00',
            {fill:'#00fffc', fontSize: 35, font:'DJB Get Digital'},this);

        gameController.timerWorking(2 * 60);

        gameController.timerWorkingSignal.add((minutes, seconds) => {
            this.timer.text = minutes + ':' + seconds;
        }, this);
    }
}