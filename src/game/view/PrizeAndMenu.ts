import {Atlases} from '../../assets';
import Group = Phaser.Group;

export class PrizeAndMenu extends Group {
    private rewardCenter: Phaser.Sprite;
    private menuBar: Phaser.Sprite;

    constructor(g: Phaser.Game,x: number, y: number, parent) {
        super(g,parent);
        this.initPrizesMenuGroup(x, y);
        this.initPrizesMenuElements();
    }

    private initPrizesMenuGroup(x: number, y: number): void {
        this.x = x;
        this.y = y
    }

    private initPrizesMenuElements(): void {
        this.rewardCenter = this.game.add.sprite(0,0,Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.RewardCenter,this);

        this.menuBar = this.game.add.sprite(0,200,Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.MenuPanel,this);
    }
}