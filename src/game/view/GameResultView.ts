import {Atlases} from '../../assets';
import {GameController} from '../controller/GameController';

export class GameResultView extends Phaser.Group {
    private winLoseRectangleBg: Phaser.Sprite;
    private winLoseText: Phaser.Text;
    private winMoneyIc: Phaser.Sprite;

    constructor(g: Phaser.Game, x: number, y: number, gameController: GameController, parent: Phaser.Group) {
        super(g, parent);
        this.init(x, y, gameController);
    }

    private init(x: number, y: number, gameController: GameController): void {
        this.initPosition(x, y);
        this.initElements(gameController);
    }

    private initPosition(x: number, y: number): void {
        this.x = x;
        this.y = y;
    }

    private initElements(gameController: GameController): void {
        this.winLoseRectangleBg = this.game.add.sprite(167,17,Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.RoundedRectangle,this);
        //in psd something doesn't correct
        this.winLoseText = this.game.add.text(this.winLoseRectangleBg.x + 26, this.winLoseRectangleBg.y,'YOU WON \t\t 999!',
            {font:'SansPosterBoldJL', fontSize: 40, fill:'#fcc74b'},this);

        this.winMoneyIc = this.game.add.sprite(423,this.winLoseRectangleBg.y,Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.UserMoney,this);

        this.winMoneyIc.visible = this.winLoseText.visible = false;

        gameController.gameOver.addOnce(() => {
            this.winMoneyIc.visible = this.winLoseText.visible = true;

        }, this);

    }

}