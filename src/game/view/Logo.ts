import {Atlases} from '../../assets';
import Group = Phaser.Group;

export class Logo extends Group{

    constructor(g: Phaser.Game,x: number, y: number,parent: Phaser.Group) {
        super(g,parent);
        this.initPosition(x, y);
        this.initLogoElements();
    }

    private initPosition(x: number, y: number): void {
        this.x = x;
        this.y = y;
    }

    private initLogoElements(): void {
        const logoArrow = this.game.add.sprite(38,0,Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.LogoDesignEl, this);

        const logoWordFinder = this.game.add.sprite(163,20,Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.LogoWordFinder, this);

        const zoomInW = this.game.add.sprite(113,4,Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.SearchW, this);

    }
}