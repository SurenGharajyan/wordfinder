import {Atlases} from '../../assets';
import {ScoreAndTimer} from './ScoreAndTimer';
import Group = Phaser.Group;
import {Letter} from './model/Letter';
import {GameController} from '../controller/GameController';
import {GameResultView} from './GameResultView';

export class LetterView extends Group {
    private isLetterSelected: boolean;
    private unselectedLetterBgExample: Phaser.Sprite;
    private letter: Phaser.Text;
    private gamingAreaBg: Phaser.Sprite;
    private penultimateSelectedLetter: Phaser.Sprite;
    private selectedLetters: Phaser.Sprite[] = [];
    private bondsLetterGraphic: Phaser.Graphics;
    private iIndexSelectedLetters: number;
    private jIndexSelectedLetters: number;
    private lettersBg: Phaser.Sprite[][] = [ [], [], [], [] ];
    private userWord: string = '';
    private letterGroup: Phaser.Group;
    private readonly words;
    private scoreAndTimer: ScoreAndTimer;
    constructor(g: Phaser.Game, scoreAndTimer: ScoreAndTimer, gameResultView: GameResultView, words, gameController: GameController, parent: Phaser.Group) {
        super(g,parent);
        this.words = words;
        this.scoreAndTimer = scoreAndTimer;
        this.initGamePosition();
        this.initElements(gameController);
    }

    private initGamePosition(): void {
        this.y = this.scoreAndTimer.y + this.scoreAndTimer.height;
        this.x = 0;
    }

    private initElements(gameController: GameController): void {
        this.isLetterSelected = false;

        this.gamingAreaBg = this.game.add.sprite(228,80,Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.GroupLetters,this);

        this.unselectedLetterBgExample = this.game.add.sprite(0,0,Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.UnselecedLetter,this);
        this.unselectedLetterBgExample.visible = false;

        this.initLetterGroup();
        this.initLettersAndBg(gameController);
        this.unselectedLetterBgExample.destroy();
    }

    private initLetterGroup(): void {
        this.letterGroup = this.game.add.group(this);
        this.letterGroup.x = this.gamingAreaBg.x + 7;
        this.letterGroup.y = 88;
    }

    private initLettersAndBg(gameController: GameController): void {
        this.bondsLetterGraphic = this.game.add.graphics(0,0,this.letterGroup);
        for (let i = 0; i < 4; i++) {
            for (let j = 0; j < 4; j++) {
                const letter = new Letter(this.game,
                    j * (4 + this.unselectedLetterBgExample.width),
                    i * (3 + this.unselectedLetterBgExample.height)
                ,this.letterGroup);
                this.lettersBg[i].push(letter.letterBgImage);
                this.letter = letter.letterSymbol;
                this.letter.x = this.lettersBg[i][j].centerX - this.letter.width / 2;
                this.letter.y = this.lettersBg[i][j].centerY - this.letter.height / 2;

                this.lettersBg[i][j].inputEnabled = true;
                this.lettersBg[i][j].input.useHandCursor = true;
                this.lettersBg[i][j].events.onInputDown.add(this.handleLetterDown
                    .bind(this, this.lettersBg[i][j], this.letter.text.toString(), i, j), this);

                this.lettersBg[i][j].events.onInputUp.add(this.handleLetterInpUp.bind(this, gameController), this);
                this.lettersBg[i][j].events.onInputOver.add(this.handleOverLetter
                    .bind(this, this.lettersBg[i][j], this.letter.text.toString(), i, j), this);

                this.lettersBg[i][j].events.onInputOut.add(this.handleOutLetter.bind(this, gameController), this);

                this.lettersBg[i][j].bringToTop();
                this.letter.bringToTop();
            }
        }
    }

    private handleOverLetter(letterBg: Phaser.Sprite, letter: string, i: number, j: number): void {
        if (this.isLetterSelected) {
            if (letterBg.frameName != Atlases.ImagesGameElements.Frames.SelectedLetter && this.penultimateSelectedLetter != null
                && (this.penultimateSelectedLetter.x === letterBg.x || this.penultimateSelectedLetter.y === letterBg.y)
                && (Math.abs(this.iIndexSelectedLetters - i) <= 1 && Math.abs(this.jIndexSelectedLetters - j) <= 1)
            ) {
                this.bondsLetterGraphic.beginFill();
                this.bondsLetterGraphic.lineStyle(14,0xFFF600);
                this.bondsLetterGraphic.lineTo(letterBg.centerX,letterBg.centerY);
                this.bondsLetterGraphic.endFill();
                this.iIndexSelectedLetters = i;
                this.jIndexSelectedLetters = j;
                this.penultimateSelectedLetter = letterBg;
                this.addSelectedLetter(letterBg, letter);
                letterBg.frameName = Atlases.ImagesGameElements.Frames.SelectedLetter;
            }
        }
    }

    private handleOutLetter(gameController: GameController): void {
        if (this.letterGroup.getBounds().contains(this.game.input.activePointer.x,this.game.input.activePointer.y)) return;
        gameController.deleteAllSelected(this.bondsLetterGraphic, this.selectedLetters);
        this.userWord = '';
        this.iIndexSelectedLetters = this.jIndexSelectedLetters = undefined;
    }

    private handleLetterDown(letterBg: Phaser.Sprite, letter: string, i: number, j: number): void {
        this.isLetterSelected = true;
        this.penultimateSelectedLetter = letterBg;
        this.iIndexSelectedLetters = i;
        this.jIndexSelectedLetters = j;
        this.addSelectedLetter(letterBg, letter);
        this.bondsLetterGraphic.visible = true;
        this.bondsLetterGraphic.beginFill();
        this.bondsLetterGraphic.moveTo(letterBg.centerX,letterBg.centerY);
        this.bondsLetterGraphic.lineStyle(14,0xFFF600);
        this.bondsLetterGraphic.lineTo(letterBg.centerX,letterBg.centerY);
        this.bondsLetterGraphic.endFill();
        letterBg.frameName = Atlases.ImagesGameElements.Frames.SelectedLetter;
    }

    private handleLetterInpUp(gameController: GameController): void {
        this.isLetterSelected = false;
        gameController.checkWord(this.words, this.userWord, this.scoreAndTimer.userScore);
        gameController.deleteAllSelected(this.bondsLetterGraphic, this.selectedLetters);
        this.userWord = '';

    }

    private addSelectedLetter(backgroundImage: Phaser.Sprite, letter: string): void {
        this.userWord += letter;
        this.selectedLetters.push(backgroundImage);
    }



}