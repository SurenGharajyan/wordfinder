import {Atlases} from '../../assets';
import Group = Phaser.Group;
import {Util} from '../../utils/util';
import LinkUtils = Util.LinkUtils;

export class TopDataOfUsers extends Group {
    private moreGames: Phaser.Sprite;
    private topBackground: Phaser.Graphics;
    private plusCoins: Phaser.Sprite;
    private prizeCenterIcon: Phaser.Sprite;
    private coinsText: Phaser.Text;
    private moneyText: Phaser.Text;
    private menuIc: Phaser.Sprite;
    private _menuIconClick: Phaser.Signal;

    constructor(g: Phaser.Game,x: number, y: number, parent?: Phaser.Group) {
        super(g,parent);
        this.initPosition(x,y);
        this.initSignals();
        this.topBackground = this.game.add.graphics(0,0,this);
        this.initTopUserData();
        this.initEvents()
    }

    private initPosition(x: number, y: number): void {
        this.x = x;
        this.y = y;
    }

    private initSignals(): void {
        this._menuIconClick = new Phaser.Signal();
    }

    private initTopUserData(): void {
        const coinMoneyBg = this.game.add.graphics(212,28,this);
        coinMoneyBg.beginFill(0x683717);
        coinMoneyBg.drawRect(0,0,140,38);
        coinMoneyBg.endFill();

        coinMoneyBg.beginFill(0x683717);
        coinMoneyBg.drawRect(235,0,160,38);
        coinMoneyBg.endFill();


        //TODO User data get from server. For examples
        const userCoin = 9876;
        const userMoney = 456789789;

        this.coinsText = this.game.add.text(275 - this.dynamicX(userCoin) , 30, this.commaInNumber(userCoin),
            {fontSize: 22, font: 'PTYanusBoldItalicCyrillic', fill: '#ffffff',
                fontWeight: 'bold', stroke: '#391b00', strokeThickness: 6, align: 'center',boundsAlignH: "center", boundsAlignV: "middle"}, this);

        this.moneyText = this.game.add.text(512 - this.dynamicX(userMoney) , 30, this.commaInNumber(userMoney),
            {fontSize: 22, font: 'PTYanusBoldItalicCyrillic', fill: '#ffffff',
                fontWeight: 'bold', stroke: '#391b00', strokeThickness: 6, align: 'center',boundsAlignH: "center", boundsAlignV: "middle"}, this);

        const moreGamesBg = this.game.add.sprite(76,0, Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.MoreGamesBg, this);

        const coins = this.game.add.sprite(208,47,Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.UserCoin, this);
        coins.anchor.setTo(0.5,0.5);

        this.plusCoins = this.game.add.sprite(374,46,Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.AddUserCoin, this);
        this.plusCoins.anchor.setTo(0.5,0.5);

        const moneyIcon = this.game.add.sprite(445,46,Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.UserMoney, this);
        moneyIcon.anchor.setTo(0.5,0.5);

        this.prizeCenterIcon = this.game.add.sprite(637,46,Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.PrizeCenter, this);
        this.prizeCenterIcon.anchor.setTo(0.5,0.5);

        this.menuIc = this.game.add.sprite(703, 18, Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.MenuIcon, this);

        this.moreGames = this.game.add.sprite(0,0,
            Atlases.ImagesGameElements.getName(),
            Atlases.ImagesGameElements.Frames.MoreGames, this);
    }

    private initEvents(): void {
        this.plusCoins.inputEnabled
            = this.prizeCenterIcon.inputEnabled
            = this.moreGames.inputEnabled
            = this.menuIc.inputEnabled
            = true;

        this.plusCoins.input.useHandCursor
            = this.prizeCenterIcon.input.useHandCursor
            = this.moreGames.input.useHandCursor
            = this.menuIc.input.useHandCursor
            = true;
        this.moreGames.events.onInputDown.add(() => {
            LinkUtils.linkBlank.bind(this,"http://www.google.com")
        }, this);
        this.menuIc.events.onInputDown.add(() => {
            this.menuIconClick.dispatch();
        }, this);
    }

    private commaInNumber(userCoinNumber: number): string {
        const coinText = userCoinNumber.toString();
        let coins = '';
        for (let i = coinText.length; i >= 1  ; i--) {
            coins += coinText.charAt(coinText.length - i);
            if ((coinText.length - i)  < 5) {
                if ((i - 1) % 3 === 0 && i - 1 !== 0) {
                    coins += ','
                }
            } else {
                coins += '...';
                break;
            }
        }
        return coins;
    }

    private dynamicX(userPoints: number): number {
        const point = userPoints.toString();
        return (point.length < 5)
            ? point.length / 2 * 11
            : point.length / 2 * 8
    }

    get menuIconClick(): Phaser.Signal {
        return this._menuIconClick;
    }

}