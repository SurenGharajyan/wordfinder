import {Builder} from '../../build/Builder';
import {ScreenManager} from '../../manager/ScreenManager';

export class BaseScreen extends Phaser.Group {

    constructor(game: Phaser.Game) {
        super(game);
        ScreenManager.i.show(this);
    }

}