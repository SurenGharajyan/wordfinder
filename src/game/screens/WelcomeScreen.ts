import {Atlases} from '../../assets';
import {BaseScreen} from './BaseScreen';
import {Util} from '../../utils/util';
import LinkUtils = Util.LinkUtils;
import {UserData} from '../../data/model/UserData';

export class WelcomeScreen extends Phaser.Group {
    private playButton: Phaser.Sprite;
    private playerAccount: Phaser.Sprite;
    private officialRules: Phaser.Sprite;
    private _onPlayClicked: Phaser.Signal;
    constructor(game: Phaser.Game) {
        super(game);
        this.init();
    }

    private init() {
        const userData = new UserData();
        userData.isFirstTime = false;
        this.initGameBgDark();
        this.initWelcomeComponents();
        this.initPlayBtn();
        this.initPlayerAc();
        this.initOfficialRules();
        this.initSignal();
        this.initEvents();
    }

    private initGameBgDark(): void {
        const darkBackground = this.game.add.graphics(0, 0, this);
        darkBackground.beginFill(0x000000,0.7);
        darkBackground.drawRect(0, 0, this.game.world.width, this.game.world.height);
        darkBackground.endFill();
    }

    private initWelcomeComponents(): void {
        const welcomeComponents = this.game.add.sprite(0, 81,
            Atlases.ImagesWelcomePopup.getName(),
            Atlases.ImagesWelcomePopup.Frames.WelcomePopup,
            this
        );
    }

    private initPlayBtn(): void {
        this.playButton = this.game.add.sprite(305, 614,
            Atlases.ImagesWelcomePopup.getName(),
            Atlases.ImagesWelcomePopup.Frames.PlayBtn,
            this
        );

    }

    private initPlayerAc(): void {
        this.playerAccount = this.game.add.sprite(249, 674,
            Atlases.ImagesWelcomePopup.getName(),
            Atlases.ImagesWelcomePopup.Frames.AlreadyAcount,
            this
        );
    }

    private initOfficialRules(): void {
        this.officialRules = this.game.add.sprite(242, 709,
            Atlases.ImagesWelcomePopup.getName(),
            Atlases.ImagesWelcomePopup.Frames.OfficialRules,
            this
        );
    }

    private initSignal(): void {
        this._onPlayClicked = new Phaser.Signal();
    }

    private initEvents(): void {
        this.playButton.inputEnabled
            = this.playerAccount.inputEnabled
            = this.officialRules.inputEnabled = true;
        this.playButton.input.useHandCursor
            = this.playerAccount.input.useHandCursor
            = this.officialRules.input.useHandCursor = true;

        this.playButton.events.onInputDown.addOnce(() => {
            this.onPlayClicked.dispatch();
        }, this);

        this.playerAccount.events.onInputDown.add(() => {
            console.log('playerAccount');
            LinkUtils.linkBlank("http://www.google.com");
        }, this);

        this.officialRules.events.onInputDown.add(() => {
            LinkUtils.linkBlank("http://www.google.com");
        }, this);

    }

    public get onPlayClicked(): Phaser.Signal {
        return this._onPlayClicked;
    }
}