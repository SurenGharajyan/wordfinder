import {BaseScreen} from './BaseScreen';
import {Atlases} from '../../assets';
import {Util} from '../../utils/util';
import MathUtils = Util.MathUtils;
import Signal = Phaser.Signal;
import {TopPlayerLayout} from '../view/model/TopPlayerLayout';
import {TopPlayer} from '../../data/model/TopPlayer';

export class LeadboardScreen extends BaseScreen {
    private _onPlayClicked: Phaser.Signal;
    private _onCloseClick: Phaser.Signal;
    private placeName: Phaser.Text;
    private placeNames = ['PLANET EARTH','MARS','JUPITER','NEPTUNE','SATURN','MERCURY'];
    private rightArrow: Phaser.Sprite;
    private leftArrow: Phaser.Sprite;
    private close: Phaser.Sprite;
    private playBtn: Phaser.Sprite;
    private infoBtn: Phaser.Sprite;
    get onCloseClick(): Phaser.Signal {
        return this._onCloseClick;
    }

    constructor(game: Phaser.Game) {
        super(game);
        this.init();
    }

    private init() {
        this.initGameBgDark();
        this.initSignals();
        this.initElements();
        this.initEvents();
    }

    private initGameBgDark(): void {
        const darkBackground = this.game.add.graphics(0, 0, this);
        darkBackground.beginFill(0x000000,0.7);
        darkBackground.drawRect(0, 0, this.game.world.width, this.game.world.height);
        darkBackground.endFill();
    }

    private initSignals(): void {
        this._onPlayClicked = new Phaser.Signal();
        this._onCloseClick = new Phaser.Signal();
    }

    private initElements(): void {
        const background = this.game.add.sprite(111, 239,
            Atlases.ImagesLeadboardPopup.getName(),
            Atlases.ImagesLeadboardPopup.Frames.LeadboardBg,
            this
        );

        const title = this.game.add.sprite(191, 191,
            Atlases.ImagesLeadboardPopup.getName(),
            Atlases.ImagesLeadboardPopup.Frames.Leaderboard,
            this
        );

        this.close = this.game.add.sprite(629, 210,
            Atlases.ImagesLeadboardPopup.getName(),
            Atlases.ImagesLeadboardPopup.Frames.CloseX,
            this
        );

        this.leftArrow = this.game.add.sprite(199, 302,
            Atlases.ImagesLeadboardPopup.getName(),
            Atlases.ImagesLeadboardPopup.Frames.LeftArrow,
            this
            );

        const placeNameBg = this.game.add.sprite(234, 282,
            Atlases.ImagesLeadboardPopup.getName(),
            Atlases.ImagesLeadboardPopup.Frames.TournamentBg,
            this
        );

        this.placeName = this.game.add.text(383, 316,
            this.placeNames[0],
            {fontSize: 36, font:'ArialRoundedMTBold', fill: '#ffffff'},
            this
        );
        this.placeName.anchor.setTo(0.5);

        this.rightArrow = this.game.add.sprite(550, 302,
            Atlases.ImagesLeadboardPopup.getName(),
            Atlases.ImagesLeadboardPopup.Frames.RightArrow,
            this
        );

        const lastScoreBg = this.game.add.sprite(141, 358,
            Atlases.ImagesLeadboardPopup.getName(),
            Atlases.ImagesLeadboardPopup.Frames.ScoreTodayBestPlayers,
            this
        );
        // TODO From UserData must GET lastScore, but now some value
        const lastScore = this.game.add.text(391, 379,
            'Last Score:' + Math.floor(Math.random() * 100),
            {fontSize: 30, font:'ArialRoundedMTBold', fill: '#ffffff'},
            this
        );
        lastScore.setShadow(2, 2, 'rgba(0,0,0,0.5)', 3);
        lastScore.anchor.setTo(0.5);

        const topPLayersBg = this.game.add.sprite(141, 400,
            Atlases.ImagesLeadboardPopup.getName(),
            Atlases.ImagesLeadboardPopup.Frames.ScoreTodayBestPlayers,
            this
        );

        const topPlayer = this.game.add.text(391, 419,
            'Top Players Today',
            {fontSize: 30, font:'ArialRoundedMTBold', fill: '#ffffff'},
            this
        );
        topPlayer.setShadow(2, 2, 'rgba(0,0,0,0.5)', 3);
        topPlayer.anchor.setTo(0.5);

        this.initTopPlayers();

        // TODO add new elements and his algorithms

        const playBtnStars = this.game.add.sprite(214, 778,
            Atlases.ImagesLeadboardPopup.getName(),
            Atlases.ImagesLeadboardPopup.Frames.PlayBtnStar,
            this
        );

        this.playBtn = this.game.add.sprite(264, 767,
            Atlases.ImagesLeadboardPopup.getName(),
            Atlases.ImagesLeadboardPopup.Frames.PlayBtn,
            this
        );

        this.infoBtn = this.game.add.sprite(339, 861,
            Atlases.ImagesLeadboardPopup.getName(),
            Atlases.ImagesLeadboardPopup.Frames.InfoBtn,
            this
        );

    }

    private initTopPlayers(): void {
        const topPlayersGroup = this.game.add.group(this);
        topPlayersGroup.x = 140;
        topPlayersGroup.y = 446;
        this.initMask(topPlayersGroup);
        const topPlayersList : TopPlayer[] = [];

        // TODO from server must GET info about top players
        // TODO top players count must getting from server too. Now some value
        // TODO It must be topPlayersList.length
        for (let i = 0; i < 8; i++) {
            topPlayersList.push(
                new TopPlayer(
                    Atlases.ImagesLeadboardPopup.Frames.CloseX,
                    'Jack ' + (i + 1),
                    'Country ' + (i + 1),
                    Math.floor(Math.random() * 1000))
            );
        }
        for (let i = 0; i < topPlayersList.length; i++) {
            const topPLayerLayout = new TopPlayerLayout(this.game,0,3 + i * 58, topPlayersList[i], topPlayersGroup)
        }
    }

    private initEvents(): void {
        this.close.inputEnabled
            = this.rightArrow.inputEnabled
            = this.leftArrow.inputEnabled
            = this.playBtn.inputEnabled
            = this.infoBtn.inputEnabled
            = true;
        this.close.input.useHandCursor
            = this.rightArrow.input.useHandCursor
            = this.leftArrow.input.useHandCursor
            = this.playBtn.input.useHandCursor
            = this.infoBtn.input.useHandCursor
            = true;
        this.close.events.onInputDown.add(() => {
            this._onCloseClick.dispatch();
        }, this);
        let index = 0;
        this.rightArrow.events.onInputDown.add(() => {
            ++index;
            if (index === this.placeNames.length) {
                index = 0;
            }
            this.placeName.text = this.placeNames[index];
        }, this);

        this.leftArrow.events.onInputDown.add(() => {
            --index;
            if (index === -1) {
                index = this.placeNames.length - 1;
            }
            this.placeName.text = this.placeNames[index];
        }, this);

        this.playBtn.events.onInputDown.add(() => {
           this._onPlayClicked.dispatch();
        }, this);

        this.infoBtn.events.onInputDown.add(() => {
            console.log('I don\'t know what to say');
        })
    }

    private initMask(group: Phaser.Group): void {
        const mask = this.game.add.graphics(0,0, group);
        mask.beginFill(0xffffff);
        mask.drawRect(0,0,502,255);
        mask.endFill();
        group.mask = mask;
    }

    get onPlayClicked(): Phaser.Signal {
        return this._onPlayClicked;
    }

    protected disposeSignals(): void {
        this._onPlayClicked.dispose();
    }

    public destroy(): void {
        this.disposeSignals();
        super.destroy();
    }
}