import {Atlases} from '../../assets';

export class GameController {
    private _gameOver: Phaser.Signal;
    private _timerWorkingSignal: Phaser.Signal;
    private interval: any;

    public get gameOver(): Phaser.Signal {
        return this._gameOver;
    }

    public get timerWorkingSignal(): Phaser.Signal {
        return this._timerWorkingSignal;
    }

    public stopInterval(): void {
        clearInterval(this.interval)
    }

    public timerWorking(duration: number): void {
        let minutes: any, seconds, time = duration;
        this.interval = setInterval(() => {
            minutes = Math.floor(time / 60);
            seconds = Math.floor(time % 60);

            // minutes = minutes < 10 ? '0' + minutes : minutes;
            seconds = seconds < 10 ? '0' + seconds : seconds;

            this._timerWorkingSignal.dispatch(minutes, seconds);
            if (time-- <= 0) {
                //Time is over
                // time = duration;
                this._gameOver.dispatch();
                clearInterval(this.interval);
            }
        }, 1000);
    }


    public checkWord(list, userWord: string, userScore: Phaser.Text): void {
        // console.log(userWord + ' have it? = ' + list[userWord]);
        if (list[userWord]) {
            // && this.userWord.length > 2
            delete list[userWord];
            this.scoreUp(userWord, userScore);
        }
    }

    public deleteAllSelected(bonds: Phaser.Graphics, selectedLetters: Phaser.Sprite[]): void {
        for (let i = 0; i < selectedLetters.length ; i++) {
            selectedLetters[i].frameName = Atlases.ImagesGameElements.Frames.UnselecedLetter;
        }
        bonds.clear();
        bonds.visible = false;
        selectedLetters = [];
    }

    public scoreUp(word: string, userScore: Phaser.Text): void {
        const userScoreInNumber = userScore.text.toString();
        let score = parseInt(userScoreInNumber);
        score += word.length;
        userScore.text = score.toString();
    }

    constructor() {
        this.init();
    }

    private init(): void {
        this.initSignals();
    }

    private initSignals(): void {
        this._gameOver = new Phaser.Signal();
        this._timerWorkingSignal = new Phaser.Signal
    }


}