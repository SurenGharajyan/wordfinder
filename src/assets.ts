/* AUTO GENERATED FILE. DO NOT MODIFY. YOU WILL LOSE YOUR CHANGES ON BUILD. */

export namespace Images {
    export class ImagesBackgroundTemplate {
        static getName(): string { return 'background_template'; }

        static getPNG(): string { return require('assets/images/background_template.png'); }
    }
    export class ImagesBgPopup {
        static getName(): string { return 'bg popup'; }

        static getPNG(): string { return require('assets/images/bg popup.png'); }
    }
    export class ImagesGameSleepStateBg {
        static getName(): string { return 'game_sleep_state_bg'; }

        static getPNG(): string { return require('assets/images/game_sleep_state_bg.png'); }
    }
    export class ImagesStateBg {
        static getName(): string { return 'state_bg'; }

        static getPNG(): string { return require('assets/images/state_bg.png'); }
    }
}

export namespace Spritesheets {
    export class SpritesheetsMetalslugMummy374518 {
        static getName(): string { return 'metalslug_mummy.[37,45,18,0,0]'; }

        static getPNG(): string { return require('assets/spritesheets/metalslug_mummy.[37,45,18,0,0].png'); }
        static getFrameWidth(): number { return 37; }
        static getFrameHeight(): number { return 45; }
        static getFrameMax(): number { return 18; }
        static getMargin(): number { return 0; }
        static getSpacing(): number { return 0; }
    }
}

export namespace Atlases {
    export class AtlasesPreloadSpritesArray {
        static getName(): string { return 'preload_sprites_array'; }

        static getJSONArray(): string { return require('assets/atlases/preload_sprites_array.json'); }

        static getPNG(): string { return require('assets/atlases/preload_sprites_array.png'); }
    }
    export namespace AtlasesPreloadSpritesArray {
        export enum Frames {
            PreloadBar = 'preload_bar.png',
            PreloadFrame = 'preload_frame.png',
        }
    }
    export class AtlasesPreloadSpritesHash {
        static getName(): string { return 'preload_sprites_hash'; }

        static getJSONHash(): string { return require('assets/atlases/preload_sprites_hash.json'); }

        static getPNG(): string { return require('assets/atlases/preload_sprites_hash.png'); }
    }
    export namespace AtlasesPreloadSpritesHash {
        export enum Frames {
            PreloadBar = 'preload_bar.png',
            PreloadFrame = 'preload_frame.png',
        }
    }
    export class AtlasesPreloadSpritesXml {
        static getName(): string { return 'preload_sprites_xml'; }

        static getPNG(): string { return require('assets/atlases/preload_sprites_xml.png'); }

        static getXML(): string { return require('assets/atlases/preload_sprites_xml.xml'); }
    }
    export namespace AtlasesPreloadSpritesXml {
        export enum Frames {
            PreloadBar = 'preload_bar.png',
            PreloadFrame = 'preload_frame.png',
        }
    }
    export class ImagesGameElements {
        static getName(): string { return 'game_elements'; }

        static getJSONArray(): string { return require('assets/images/game_elements.json'); }

        static getPNG(): string { return require('assets/images/game_elements.png'); }
    }
    export namespace ImagesGameElements {
        export enum Frames {
            AddUserCoin = 'add_user_coin.png',
            BgGame = 'bg_game.png',
            ExitBtn = 'exit_btn.png',
            GroupLetters = 'group_letters.png',
            LettersConcatLine = 'letters_concat_line.png',
            LineBelowLetter = 'line_below_letter.png',
            LineTrim = 'line_trim.png',
            LogoDesignEl = 'logo_design_el.png',
            LogoWordFinder = 'logo_word_finder.png',
            MenuIcon = 'menu_icon.png',
            MenuPanel = 'menu_panel.png',
            MoreGames = 'more_games.png',
            MoreGamesBg = 'more_games_bg.png',
            PrizeCenter = 'prize_center.png',
            RewardCenter = 'reward_center.png',
            RoundedRectangle = 'rounded_rectangle.png',
            RoundedRectangleTime = 'rounded_rectangle_time.png',
            SearchW = 'search_w.png',
            SelectedLetter = 'selected_letter.png',
            Timer = 'timer.png',
            TimerBg = 'timer_bg.png',
            UnselecedLetter = 'unseleced_letter.png',
            UserCoin = 'user_coin.png',
            UserMoney = 'user_money.png',
            YouBg = 'you_bg.png',
            YouScoreBg = 'you_score_bg.png',
            YouTimerBg = 'you_timer_bg.png',
            YouTimerLightBg = 'you_timer_light_bg.png',
            YouTimerTrimBg = 'you_timer_trim_bg.png',
        }
    }
    export class ImagesLeadboardPopup {
        static getName(): string { return 'leadboard_popup'; }

        static getJSONArray(): string { return require('assets/images/leadboard_popup.json'); }

        static getPNG(): string { return require('assets/images/leadboard_popup.png'); }
    }
    export namespace ImagesLeadboardPopup {
        export enum Frames {
            CloseX = 'close_x.png',
            InfoBtn = 'info_btn.png',
            LeadboardBg = 'leadboard_bg.png',
            Leaderboard = 'leaderboard.png',
            LeftArrow = 'left_arrow.png',
            PlayBtn = 'play_btn.png',
            PlayBtnStar = 'play_btn_star.png',
            RightArrow = 'right_arrow.png',
            ScoreTodayBestPlayers = 'score_today_best_players.png',
            SelectedPlaceBg = 'selected_place_bg.png',
            TournamentBg = 'tournament_bg.png',
            UnselectedPlaceBg = 'unselected_place_bg.png',
        }
    }
    export class ImagesWelcomePopup {
        static getName(): string { return 'welcome_popup'; }

        static getJSONArray(): string { return require('assets/images/welcome_popup.json'); }

        static getPNG(): string { return require('assets/images/welcome_popup.png'); }
    }
    export namespace ImagesWelcomePopup {
        export enum Frames {
            AlreadyAcount = 'already_acount.png',
            OfficialRules = 'official_rules.png',
            PlayBtn = 'play_btn.png',
            WelcomePopup = 'welcome_popup.png',
        }
    }
}

export namespace Audio {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}

export namespace Audiosprites {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}

export namespace GoogleWebFonts {
    export const Barrio: string = 'Barrio';
}

export namespace CustomWebFonts {
    export class FontsArialRoundedMTBold {
        static getName(): string { return 'ArialRoundedMTBold'; }

        static getFamily(): string { return 'ArialRoundedMTBold'; }

        static getCSS(): string { return require('!file-loader?name=assets/fonts/[name].[ext]!assets/fonts/ArialRoundedMTBold.css'); }
        static getTTF(): string { return require('!file-loader?name=assets/fonts/[name].[ext]!assets/fonts/ArialRoundedMTBold.ttf'); }
    }
    export class FontsPTYanusBoldItalicCyrillic {
        static getName(): string { return 'PTYanusBoldItalicCyrillic'; }

        static getFamily(): string { return 'PTYanusBoldItalicCyrillic'; }

        static getCSS(): string { return require('!file-loader?name=assets/fonts/[name].[ext]!assets/fonts/PTYanusBoldItalicCyrillic.css'); }
        static getTTF(): string { return require('!file-loader?name=assets/fonts/[name].[ext]!assets/fonts/PTYanusBoldItalicCyrillic.ttf'); }
    }
    export class FontsSansPosterBoldJL {
        static getName(): string { return 'SansPosterBoldJL'; }

        static getFamily(): string { return 'SansPosterBoldJL'; }

        static getCSS(): string { return require('!file-loader?name=assets/fonts/[name].[ext]!assets/fonts/SansPosterBoldJL.css'); }
        static getTTF(): string { return require('!file-loader?name=assets/fonts/[name].[ext]!assets/fonts/SansPosterBoldJL.ttf'); }
    }
}

export namespace BitmapFonts {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}

export namespace JSON {
    export class JsonEnglishWords {
        static getName(): string { return 'englishWords'; }

        static getJSON(): string { return require('assets/json/englishWords.json'); }
    }
}

export namespace XML {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}

export namespace Text {
    export class TextWords {
        static getName(): string { return 'words'; }

        static getTXT(): string { return require('assets/text/words.txt'); }
    }
}

export namespace Scripts {
    export class ScriptsBlurX {
        static getName(): string { return 'BlurX'; }

        static getJS(): string { return require('assets/scripts/BlurX.js'); }
    }
    export class ScriptsBlurY {
        static getName(): string { return 'BlurY'; }

        static getJS(): string { return require('assets/scripts/BlurY.js'); }
    }
}
export namespace Shaders {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}
export namespace Misc {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}
