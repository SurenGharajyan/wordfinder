import {GameScreen} from '../game/screens/GameScreen';
import {TopDataOfUsers} from '../game/view/TopDataOfUsers';
import {Images} from '../assets';
import {PrizeAndMenu} from '../game/view/PrizeAndMenu';
import {WelcomeScreen} from '../game/screens/WelcomeScreen';
import {UserData} from '../data/model/UserData';
import {BaseScreen} from '../game/screens/BaseScreen';
import {GameController} from '../game/controller/GameController';
import {LeadboardScreen} from '../game/screens/LeadboardScreen';

export default class Game extends Phaser.State {
    private currentScreen: BaseScreen;
    public preload() : void {
        console.log('PRELOAD');
    }
    public create() : void {
        this.initialize();
        console.log('CREATE');
    }

    private initialize(): void {
        this.game.add.sprite(0, 0, Images.ImagesStateBg.getName(), null, this.world);
        const gameController = new GameController();
        const userData = new UserData();
        // TODO GET from server all data for this object
        this.initImmobile(gameController);

        // TODO for example
        userData.isFirstTime = true;
        userData.isFirstTime ? this.showWelcomeScreen(gameController) : this.showGameScreen(gameController);
    }

    private showGameScreen(gameController: GameController): void {
        if (this.currentScreen) this.currentScreen.destroy();
        this.currentScreen = new GameScreen(this.game, gameController);
        gameController.gameOver.add(() => {
            setTimeout(() => {
                this.showLeadboardScreen(gameController);
            },2000);
        }, this.currentScreen);
    }

    private showWelcomeScreen(gameController: GameController): void {
        if (this.currentScreen) this.currentScreen.destroy();
        this.currentScreen = new WelcomeScreen(this.game);
        (this.currentScreen as WelcomeScreen).onPlayClicked.addOnce(() => {
            gameController.stopInterval();
            this.showGameScreen(gameController);
        }, this);
    }

    private showLeadboardScreen(gameController: GameController): void {
        if (this.currentScreen) this.currentScreen.destroy();
        this.currentScreen = new LeadboardScreen(this.game);
        (this.currentScreen as LeadboardScreen).onPlayClicked.addOnce(() => {
            gameController.stopInterval();
            this.showGameScreen(gameController);
        }, this);
        (this.currentScreen as LeadboardScreen).onCloseClick.addOnce(() => {
            this.showWelcomeScreen(gameController);
        }, this);
    }

    private initImmobile(gameController: GameController): void {
        const topOfGame = new TopDataOfUsers(this.game,0,0,this.world);
        topOfGame.menuIconClick.add(() => {
        }, this);
        const prizeAndMenu = new PrizeAndMenu(this.game,0,753, this.world);
    }
}


